// import Typography from "@mui/material/Typography";
import PushPinIcon from "@mui/icons-material/PushPin";
import { grey } from "@mui/material/colors";

import "./styles/post.css";

function Post({ postMessage, bgColor }) {
  return (
    <div
      className="mt-3 me-3 p-3 rounded shadow"
      style={{ backgroundColor: bgColor }}
    >
      <div className="d-flex justify-content-end">
        <PushPinIcon sx={{ fontSize: 18, color: grey[50] }} />
      </div>
      {/* <i>
        <Typography variant="button">{postMessage}</Typography>
      </i> */}
      <span className="nb-post-txt">{postMessage}</span>
    </div>
  );
}

export default Post;
