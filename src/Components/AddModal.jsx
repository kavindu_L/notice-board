import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useState } from "react";
import { addPost, closeBackdrop } from "../topicsSlice";
import { useSelector, useDispatch } from "react-redux";

function AddModal({ topic }) {
  const { backdrop } = useSelector(store => store.topics);
  const [notice, setNotice] = useState("");
  const dispatch = useDispatch();

  function handleAddButton() {
    if (notice.length === 0) {
      alert("Please add a valid notice !");
      return;
    }

    dispatch(addPost({ topic, notice }));
    dispatch(closeBackdrop());
    setNotice("");
  }

  const handleClose = () => {
    dispatch(closeBackdrop());
  };

  return (
    <Dialog open={backdrop} onClose={handleClose}>
      <DialogTitle>Add a notice to '{topic}' board</DialogTitle>
      <DialogContent>
        <DialogContentText>Please type your notice here</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          fullWidth
          variant="standard"
          value={notice}
          onChange={e => setNotice(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleAddButton}>Add</Button>
      </DialogActions>
    </Dialog>
  );
}

export default AddModal;
