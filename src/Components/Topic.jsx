import { Link } from "react-router-dom";
import PushPinIcon from "@mui/icons-material/PushPin";
import { useDispatch } from "react-redux";
import { setBgColor } from "../topicsSlice";

import "./styles/topic.css";

function Topic({ topicName, bgColor }) {
  const dispatch = useDispatch();

  function handleLinkClick() {
    dispatch(setBgColor({ topicName, bgColor }));
  }

  return (
    <Link className="nav-link" to={`/${topicName}`}>
      <div
        className="mt-3 me-3 p-3 rounded shadow"
        style={{ backgroundColor: bgColor }}
        onClick={handleLinkClick}
      >
        <div className="d-flex justify-content-end">
          <PushPinIcon fontSize="small" />
        </div>
        <h2 className="nb-home-topic">{topicName}</h2>
        {/* <Typography variant="h2">{topicName}</Typography> */}
      </div>
    </Link>
  );
}

export default Topic;
