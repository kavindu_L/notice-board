import Topic from "./Topic";
import { useSelector } from "react-redux";

function Home() {
  const { dataArr } = useSelector(store => store.topics);
  const topics = dataArr.map(data => data.topic);

  const { colors } = useSelector(store => store.topics);

  return (
    <div className="d-flex flex-wrap px-3">
      {topics.map((topic, index) => (
        <Topic topicName={topic} key={topic} bgColor={colors[index]} />
      ))}
    </div>
  );
}

export default Home;
