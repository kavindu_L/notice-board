import Post from "./Post";
import { useSelector, useDispatch } from "react-redux";
import { useParams, Link } from "react-router-dom";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { setBackdrop } from "../topicsSlice";
import AddModal from "./AddModal";

import "./styles/posts.css";
import { colors } from "../data";

function Posts() {
  const { topic } = useParams();

  const dispatch = useDispatch();

  const { dataArr } = useSelector(store => store.topics);
  // console.log(dataArr.filter(d => d.topic === "Vehicles").posts);

  // const bgColor = dataArr.find(data => data.topic === topic).bgColor;

  // const colors = data;

  const posts = dataArr.find(data => data.topic === topic).posts;
  const topicId = dataArr.find(data => data.topic === topic).id;
  const bgColor = colors[topicId - 1];

  const handleClickOpen = () => {
    dispatch(setBackdrop());
  };

  return (
    <div>
      <h2 className="nb-posts-topic">{topic}</h2>
      <div className="d-flex flex-wrap px-3">
        {posts.map(post => (
          <Post postMessage={post.message} key={post.id} bgColor={bgColor} />
        ))}
      </div>

      <div>
        <div className="position-absolute bottom-0 start-0 m-3 sticky-bottom">
          <Link className="nav-link" to="/">
            <ArrowBackIcon sx={{ fontSize: 50 }} />
          </Link>
        </div>

        <div
          className="position-absolute bottom-0 end-0 m-3 sticky-bottom"
          onClick={handleClickOpen}
        >
          <AddCircleOutlineIcon sx={{ fontSize: 50, cursor: "pointer" }} />
        </div>

        <AddModal topic={topic} />
      </div>
    </div>
  );
}

export default Posts;
