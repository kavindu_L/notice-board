import React from "react";
import ReactDOM from "react-dom/client";
import NoticeBoard from "./NoticeBoard";
import { Provider } from "react-redux";
import store from "./store";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <React.StrictMode>
      <NoticeBoard />
    </React.StrictMode>
  </Provider>
);
