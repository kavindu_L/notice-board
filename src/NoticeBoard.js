import Home from "./Components/Home";
import Posts from "./Components/Posts";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import "./index.css";

function NoticeBoard() {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <div className="container mt-3">
        <center>
          <Link className="nav-link" to="/">
            <h3 className="nb-header">Notice Board</h3>
          </Link>

          {/* <Link className="nav-link" to="/">   
            <Typography variant="h3" gutterBottom sx={{ color: "#FFFFF0" }}>
              Notice Board
            </Typography>
          </Link> */}
        </center>
        <div
          className="rounded-4 overflow-auto position-relative"
          style={{
            height: "500px",
            border: "10px solid #E2DFD2",
            backgroundColor: "#FFFFF0"
          }}
        >
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/:topic" element={<Posts />} />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default NoticeBoard;
