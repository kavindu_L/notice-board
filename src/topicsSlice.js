import { createSlice } from "@reduxjs/toolkit";
import { data, colors } from "./data";

const initialState = {
  dataArr: data,
  colors,
  backdrop: false
};

const topicsSlice = createSlice({
  name: "topics",
  initialState,
  reducers: {
    setBgColor: (state, action) => {
      state.dataArr.find(
        data => data.topic === action.payload.topicName
      ).bgColor = action.payload.bgColor;
    },
    addPost: (state, { payload }) => {
      const selectedTopic = state.dataArr.find(
        data => data.topic === payload.topic
      );
      const lastId = selectedTopic.posts[selectedTopic.posts.length - 1].id;

      selectedTopic.posts.push({
        id: lastId + 1,
        topic: payload.topic,
        message: payload.notice
      });
    },
    setBackdrop: (state, action) => {
      state.backdrop = true;
    },
    closeBackdrop: (state, action) => {
      state.backdrop = false;
    }
  }
});

export const {
  setBgColor,
  addPost,
  setBackdrop,
  closeBackdrop
} = topicsSlice.actions;
export default topicsSlice.reducer;
