export const colors = [
  "dodgerblue",
  "plum",
  "pink",
  "red",
  "orange",
  "yellow",
  "green",
  "teal",
  "cyan",
  "beige",
  "aquamarine",
  "coral",
  "gainsboro"
];

export const data = [
  {
    id: 1,
    topic: "Vehicles",
    posts: [
      { id: 10, topic: "Vehicles", message: "motor cycle for sale - raj" },
      { id: 11, topic: "Vehicles", message: "Need a van for a trip - Sunil" },
      {
        id: 12,
        topic: "Vehicles",
        message: "Need a foot cycle for a drama - drama circle"
      }
    ],
    bgColor: ""
  },
  {
    id: 2,
    topic: "Apartments",
    posts: [
      {
        id: 20,
        topic: "Apartments",
        message: "Anex for 2 is available - Gihan"
      },
      {
        id: 21,
        topic: "Apartments",
        message: "Newly build house for sale - ABC company"
      },
      {
        id: 22,
        topic: "Apartments",
        message: "need a house with 2 bedrooms for a family - Sunila"
      }
    ],
    bgColor: ""
  },
  {
    id: 3,
    topic: "Businesses",
    posts: [
      {
        id: 30,
        topic: "Businesses",
        message:
          "Urgent ! need someone for invest for a profitable idea - Esala"
      },
      {
        id: 31,
        topic: "Businesses",
        message: "Imported garments available - XYZ garments"
      },
      {
        id: 32,
        topic: "Businesses",
        message:
          "stationary below market price is available(discounts for bulk purchases) - Rathana Stationary"
      }
    ],
    bgColor: ""
  },
  {
    id: 4,
    topic: "Real Estate",
    posts: [
      {
        id: 30,
        topic: "Real Etate",
        message: "Looking for free place for a business - kamj"
      },
      {
        id: 40,
        topic: "Real Etate",
        message:
          "Land near a river for sale. Suitable for hotels, restaurants etc.. - seven seas hotels"
      },
      {
        id: 41,
        topic: "Real Etate",
        message: "Valuable space in town for rent or lease - Rusiri"
      },
      {
        id: 42,
        topic: "Real Etate",
        message: "Land outside the town is needed ASAP ! - yohani"
      }
    ],
    bgColor: ""
  },
  {
    id: 5,
    topic: "Jobs",
    posts: [
      {
        id: 50,
        topic: "Jobs",
        message: "people with type setting skills are needed - harith"
      },
      {
        id: 51,
        topic: "Jobs",
        message: "I'm teaching maths for grade 6 students - Manel"
      },
      {
        id: 52,
        topic: "Jobs",
        message: "Fresh IT graduate looking for a job - fazil"
      }
    ],
    bgColor: ""
  },
  {
    id: 6,
    topic: "Personal",
    posts: [
      {
        id: 60,
        topic: "Personal",
        message: "Marrage councelor needed - Councellor center"
      }
    ]
  },
  {
    id: 7,
    topic: "For Sale",
    posts: [
      {
        id: 70,
        topic: "For Sale",
        message: "Three wheeler is for sale - Real Motores"
      },
      {
        id: 71,
        topic: "For Sale",
        message:
          "Newly purchased, unsued table set for sale - Weerasinghe furnitures"
      },
      {
        id: 72,
        topic: "For Sale",
        message: "need a used washine machine(old ones are okay) - paul"
      }
    ],
    bgColor: ""
  },
  {
    id: 8,
    topic: "For Rent",
    posts: [
      {
        id: 80,
        topic: "For Rent",
        message: "My car is for rent, for weddings, events etc"
      },
      {
        id: 81,
        topic: "For Rent",
        message: "I have studio space for rent - Sound of music"
      },
      {
        id: 82,
        topic: "For Rent",
        message: "Anyone has red wedding frock for rent? - shamali"
      }
    ],
    bgColor: ""
  },
  {
    id: 9,
    topic: "Comming Events",
    posts: [
      { id: 90, topic: "Comming Events", message: "Wayo concert next week" },
      {
        id: 91,
        topic: "Comming Events",
        message: "lecture on climate change will be held this week - leo club"
      },
      {
        id: 92,
        topic: "Comming Events",
        message: "pre school concert is this friday"
      }
    ],
    bgColor: ""
  }
];
